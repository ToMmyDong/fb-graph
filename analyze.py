import json

with open("./result.json") as f:
    result = json.load(f)

def IaU (s1, s2):
    s1 = set(s1)
    s2 = set(s2)
    return len(s1 & s2), len(s1 | s2)

graph = {}
graph["nodes"] = []
graph["links"] = []

wei = []
comment_count = 0
for x in range(len(result)):
    comment_count += len(result[x]["users"])
    graph["nodes"].append({"id":result[x]["name"],"group":1})
    for y in range(len(result)):
        if x != y:
            I, U = IaU(result[x]["users"], result[y]["users"])
            wei.append(I/U)
            if I/U > 0.005:
                graph["links"].append({"source":result[x]["name"], "target":result[y]["name"], "value":1})

print(f'Total {len(graph["nodes"])} nodes, {len(graph["links"])} edges {comment_count} comments')

# wei.sort()
# import matplotlib.pyplot as plt
# plt.plot(wei)
# plt.show()
with open("./graph.json", "w", encoding="utf-8") as f:
    json.dump(graph, f, ensure_ascii=False)

