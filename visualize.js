var svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");


var color_org = "#007bff";
var color_light = "#ffa931"

var con = svg.append("g")
             .attr("transform","translate(-200,-200)scale(1.7,1.7)");

svg.call(d3.zoom()
          .extent([[0, 0], [width, height]])
          .scaleExtent([1, 8])
          .on("zoom", zoomed));
function zoomed() {
    con.attr("transform", d3.event.transform);
}


var color = d3.scaleOrdinal(d3.schemeCategory20);
var free = true;

var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function(d) { return d.id; }).distance(function(d){return 30;}))
    .force("charge", d3.forceManyBody())
    .force("center", d3.forceCenter(width / 2, height / 2));

d3.json("./graph.json", function(error, graph) {
  if (error) throw error;

  var link = con.append("g")
      .attr("class", "links")
    .selectAll("line")
    .data(graph.links)
    .enter().append("line")
      .attr("stroke-width", function(d) { return Math.sqrt(d.value); });

  var node = con.append("g")
      .attr("class", "nodes")
    .selectAll("g")
    .data(graph.nodes)
    .enter().append("g")
    .attr("id", function(d, i){
        return "nid_" + i.toString(); 
    });
    
  var circles = node.append("circle")
      .attr("r", 3)
      .attr("fill", function(d) { return color_org; })
      .attr("id", function(d,i) {
          return "cid_" + i.toString();
      })
      .call(d3.drag()
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended));

  var lables = node.append("text")
      .text(function(d) {
        return d.id;
      })
      .attr('x', 6)
      .attr('y', 3)
      .style("font-size", "7px");

  node.append("title")
      .text(function(d) { return d.id; });

  simulation
      .nodes(graph.nodes)
      .on("tick", ticked);

  simulation.force("link")
      .links(graph.links)

  function ticked_tran() {
    link
        .transition()
        .duration(1000)
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node
        .transition()
        .duration(1000)
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        })
  }

  function ticked() {
    link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        })
  }
    var panel = d3.select(".panel")
        .selectAll("div")
        .data(graph.nodes)
        .enter()
        .append("button")
        .text(function(d){
            return d.id;
        })
        .attr("id", function(d, i) {
            return "bid_" + i.toString();
        })
        .attr("class", function(d){return "k-button k-primary"});

    var start_btn = d3.select(".panel")
                    .append("button").text("start")
                    .attr("class", "k-button k-primary")
                    .attr("id", "start")

    var on_count = 0;
    var on_set = new Set();
    panel.on("click", function(d) {
        bid = this.id;
        cid = "c" + bid.substr(1);
        nid = "n" + bid.substr(1);
        let circle = d3.select("#"+cid)
        //console.log(d3.select("#"+nid).x);
        //graph.nodes[d.index].fx = 0;
        
        if (circle.attr("fill") == color_org) {
            if (on_count < 3) {
                circle.attr("fill", color_light)
                .attr("r", 5);
                on_count += 1;
                on_set.add(d.index);
            }
        } else {
            circle.attr("fill", color_org)
            .attr("r", 3);
            on_count -= 1
            on_set.delete(d.index);
        }
    });

    function prep () {
        let n = graph.nodes.length;
        dist = new Array(n);
        for (let i=0; i<n; i++) {
            dist[i] = new Array(n);
            dist[i].fill(1000);
            dist[i][i] = 0;
        }
        for (let i=0; i<graph.links.length;i++) {
            let f = graph.links[i]["source"]["index"];
            let t = graph.links[i]["target"]["index"];
            dist[f][t] = 1;
        }
        for (let k=0; k<n; k++) {
            for (let i=0; i<n; i++) {
                for (let j=0; j<n; j++) {
                    if (dist[i][k] + dist[k][j] < dist[i][j]) {
                        dist[i][j] = dist[i][k] + dist[k][j];
                    }
                }
            }
        }

        let weight = new Array(n);
        weight.fill(0);
        on_set.forEach(function(idx) {
            for (let i=0; i<n; i++) {
                weight[i] += dist[idx][i];
            }
        });
        on_set.forEach(function(idx) {
            weight[idx] = 0;
        });

        let ord = new Array(n);
        for (let i=0; i<n; i++) {
            ord[i] = i;
        }
        ord.sort(function(x,y) {
            return weight[x] < weight[y] ? -1 : 1;
        });

        let x_pos = 0;
        let y_pos = 0;
        let l_width = 50;
        let l_height = 50;
        let col_sz = 3;
        let x_shift = 150;
        let y_shift = 300;
        free = false;
        for (let i=0; i<n; i++) {
            console.log(i);
            let nid = ord[i];
            graph.nodes[nid].x = x_pos * l_width + x_shift;
            graph.nodes[nid].y = y_pos * l_height + y_shift;
            y_pos += 1;
            if (y_pos >= col_sz) {
                x_pos += 1;
                y_pos = 0;
            }
        }
        ticked_tran();
    }

    d3.select("#start").on("click", function(d) {
        console.log("start");
        prep();
    });
});

function dragstarted(d) {
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  if (free) {
      d.fx = d.x;
      d.fy = d.y;
  }
}

function dragged(d) {
  if (free) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
  }
}

function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  if (free) {
      //d.fx = null;
      //d.fy = null;
  }
}

