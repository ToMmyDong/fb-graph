from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup

import time
import random

import csv
import json

import configparser

config = configparser.ConfigParser()
config.read("./auth.ini")
user_name = config["TMD"]["user_name"]
password = config["TMD"]["user_password"]

result = []
with open("./result.json") as f:
    result = json.load(f)

rw = []
with open("./list.csv") as f:
    rows = csv.reader(f)
    for row in rows:
        rw.append(row)

def stop (base = 0.5):
    time.sleep(random.random() + base)


def login ():
    # try:
        # ele = WebDriverWait(driver, 10).until(
            # EC.visibility_of_element_located((By.ID, 'expanding_cta_close_button'))
        # )
    # ele.click()

    stop()
    driver.find_element_by_id("email").send_keys(user_name) 
    stop()
    driver.find_element_by_id("pass").send_keys(password) 
    stop()
    driver.find_element_by_id("loginbutton").click()
    stop()
    # except TimeoutException:
        # print('pop windows not found')

def get_name ():
    page = driver.page_source
    soup = BeautifulSoup(page, 'html.parser')
    names = soup.find_all(class_="_6qw4")
    name_set = set()
    for name in names:
        name_set.add(name.text)
    return name_set

def expand (itr):

    check = 5
    lower = 500

    not_found = False
    for i in range(itr):
        stop(1)
        lst = driver.find_elements_by_class_name("_4ssp")

        for item in lst:
            if item.text[-1] =="言":
                not_found = False
                try:
                    item.click()
                except:
                    print("Error")
                stop()
                WebDriverWait(driver, 8).until_not(EC.presence_of_element_located(
                            (By.CSS_SELECTOR, '.mls.img._55ym._55yn._55yo')))
                print("safe")
                break
        else:
            if not_found:
                break
            scroll_down = "window.scrollTo(0, document.body.scrollHeight);"
            driver.execute_script(scroll_down)
            stop()
            WebDriverWait(driver, 8).until_not(EC.presence_of_element_located(
                        (By.CSS_SELECTOR, '.mls.img._55ym._55yn._55yo')))
        if (i + 1) % check == 0:
            cur_name = get_name()
            print(f"check point {len(cur_name)}")
            if len(cur_name) >= lower:
                break
        stop()

def safe():
    with open("result.json", "w", encoding="utf8") as f:
        json.dump(result, f, ensure_ascii=False)

chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)

driver = webdriver.Chrome(chrome_options=chrome_options)
driver.get("https://www.facebook.com/")
login()


rw = rw[1:-1]
for row in rw:
    exist = -1
    for idx, itm in enumerate(result):
        if itm["name"] == row[0]:
            exist = idx

    if exist != -1:
        print("f{row[0]} exist")
        result[idx]["url"] = row[3]
        continue

    cur = {"name" : row[0]}
    driver.get(row[3])
    time.sleep(5)
    expand(20)

    name_set = get_name()
    cur["users"] = list(name_set)
    print(len(name_set))
    result.append(cur)
    safe()



